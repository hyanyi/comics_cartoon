$(document).ready(function() {

	// 全局页面进入loading
	let pageLoading=()=> {
		setTimeout(()=> {
			var index = layer.load(2, {shade: [0.1,'#fff']});
			setTimeout(()=> {
				layer.close(index);
				$('.landing').css({'opacity': '1'});
			},500);
		},100);

	}
	pageLoading();

	$('.tab-title, .left-tabm, .li-list, .unit-box').on('click', 'a', function(event) {
		$(this).addClass('select').siblings().removeClass('select');
	});

	$('.recharge-main').on('click', 'li', function(event) {
		$(this).addClass('select').siblings().removeClass('select');
	});


	$("#content").keyup(function(){

		var length = $("#result").find('span').html();
		var content_len = $("#content").val().length;
		var in_len = length-content_len;

		if(in_len >=0){
			$("#result").find('strong').html(in_len);
			$("#btn").attr("disabled",false);
		}else{
			$("#result").find('strong').html(in_len);
			$("#btn").attr("disabled",true);
			return false;
		}

	});

});


var mySwiper = new Swiper ('.swiper-container', {
	direction: 'horizontal',  //水平
	loop: true,
	pagination: '.swiper-pagination',		// 如果需要分页器
	nextButton: '.swiper-button-next',		// 如果需要前进后退按钮
	prevButton: '.swiper-button-prev',		// 如果需要前进后退按钮
	scrollbarHide: true,
	autoplay: 5000,							//可选选项，自动滑动
});

jQuery(".txtMarquee-left").slide({mainCell:".bd ul",autoPlay:true,autoPage:true,effect:"top",vis:1});

// 超过xx字符长度做省略号
function eachLength(className, number) {	
	$(className).each(function(){
		if($(this).text().length>number){
			var str = $(this).text();
			$(this).text(str.substr(0,number)+'...');
		}
	});
}
var eachLength1 = eachLength('.classify-list .p-inf', '40');
var eachLength2 = eachLength('.classify-list .p-inf-2', '20');

// 开启编辑状态
$('.editor').click(function() {
	var _this = $(this);	
	if (_this.attr('change') == "on") {
		$('.history-main').find('ul').removeClass('editor-ul');
		$('.control').fadeOut();
		_this.attr('change', '');
	} else {
		$('.history-main').find('ul').addClass('editor-ul');
		$('.control').fadeIn();
		_this.attr('change', 'on');
	}
});

// 点击开关选择按钮
$('.history-main ul').on('click', 'i', function(event) {
	var totality = $('.history-main li').length;
	var selected = $('.history-main li .hook').length+1;
	var _this = $(this);
	if (_this.hasClass('hook')) {		
		_this.removeClass('hook');
		$('.check-all i').removeClass('hook');
		if ((totality-selected) == 0) {
			$('.delete').find('i').removeClass('hook');
		}
	} else {
		_this.addClass('hook');
		$('.delete').find('i').addClass('hook');
		if (totality == selected) {
			$('.check-all').find('i').addClass('hook');
		} else {
			$('.check-all').find('i').removeClass('hook');
		}
	}
});

// 点击全选 && 删除
$('.control').on('click', 'a', function(event) {
	var _this = $(this);
	if (_this.hasClass('check-all')) {

		if (_this.attr('change') == "on") {
			_this.attr('change', '').find('i').removeClass('hook');
			$('.history-main ul').find('i').removeClass('hook');
			$('.delete').find('i').removeClass('hook');
		} else {
			_this.attr('change', 'on').find('i').addClass('hook');
			$('.history-main ul').find('i').addClass('hook');
			$('.delete').find('i').addClass('hook');
		}

	} else {

	}
});


let timeAjax=()=> {

	setTimeout(()=> {
		$('.time-box').animate({
			'height': '0'
		});
	},1000);
	
}
// timeAjax();

// 开启二维码窗口
$('.invitation .invita-box').on('click', 'a', function() {
	$('.invitation-code').fadeIn();
});

// 点击关闭二维码窗口
$('.code-close').click(function() {
	$('.invitation-code').fadeOut();
});

// 点赞状态
$('.footer-catalog').on('click', '.zan', function(event) {
	var _this = $(this);
	if (_this.hasClass('state-02')) {
		_this.removeClass('state-02');
	} else {
		_this.addClass('state-02');
	}
});

$('.catalog-btn').click(function() {
	$('.catalog-wrap').fadeIn().find('ul').animate({'bottom': '0'});
});


$('.catalog-cancel').click(function() {
	$('.catalog-wrap').fadeOut().find('ul').animate({'bottom': '-100%'});
});


// recharge

$('.success-box .btn-box').on('click', '.close-btn', function(event) {
	$('.recharge-success').fadeOut();
});

$('.more-btn').click(function() {
	var index = layer.load(2, {
		shade: [0.1,'#fff']
	});
	setTimeout(function() {

		layer.close(index);
		layer.msg('更新完成！', {time:1000});

	},1000)
});

$('.updata-a').click(function() {
	var index = layer.load(2, {
		shade: [0.1,'#fff']
	});
	setTimeout(function() {

		layer.close(index);
		layer.msg('更新完成！', {time:1000});

	},1000)
});

$('.sign-in-btn').click(function() {
	if ($(this).hasClass('have-signed-in')) {
		layer.msg('已签到,请明天再来哦~', {time:1000});
	} else {
		$('.sign-in-wrap').fadeIn();
	}
});

$('.sign-close').click(function() {
	$('.sign-in-wrap').fadeOut();
	$('.sign-in-btn').addClass('have-signed-in').html('已签到');
});

$('.collection-a').click(function(event) {
	if ($(this).hasClass('collection-on')) {
		layer.msg('取消收藏！', {time:1000});
		$(this).removeClass('collection-on');
	} else {
		$(this).addClass('collection-on');
		layer.msg('添加收藏！', {time:1000});
	}
});

// 发送短信验证码
$('.obtain-verification').on('click','a', function(){

	var _this = $(this);
	var mobile = $('.inp-phone').val();
	var mobileReg  = /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9]|17[0|1|2|3|5|6|7|8|9])\d{8}$/;
	if (mobile == "") {
		layer.msg('手机号码不能为空', {time:1000});
		$('.inp-phone').focus();
		return
	}
	if (!mobileReg.test(mobile)) {
		layer.msg('您的手机格式错误！<br/>请输入11位数移动电话号码', {time:1000});
		$('.inp-phone').focus();
		return
	}

	if (mobileReg.test(mobile)) {
		var index = layer.load(2, {
			shade: [0.1,'#fff']
		});
		setTimeout(function() {

			layer.close(index);
			layer.msg('短信发送中，请查收验证码。', {time:1000});
			time();

		},1000)

		
		// $.ajax({
		// 	url: 'http://api.qcplay.com/sms',
		// 	type: 'POST',
		// 	data: 'mobile=' + mobile + '&channel=activation&captcha=' + captcha,
		// 	success: function (json) {
		// 		layer.close(index);
		// 		if (json.result == true) {
		// 			time();
		// 			layer.msg(json.message, {time:1000});
		// 		} else {
		// 			layer.msg(json.message, {time:1000});
		// 		}
		// 	}
		// });
	};
});

$('.login-btn').on('click','a', function(){

	var _this = $(this);
	var mobile = $('.inp-phone').val();
	var verification = $('.inp-verification').val();


	var mobileReg  = /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9]|17[0|1|2|3|5|6|7|8|9])\d{8}$/;
	if (mobile == "") {
		layer.msg('手机号码不能为空', {time:1000});
		$('.inp-phone').focus();
		return
	}
	if (!mobileReg.test(mobile)) {
		layer.msg('您的手机格式错误！<br/>请输入11位数移动电话号码', {time:1000});
		$('.inp-phone').focus();
		return
	}
	if (verification == "") {
		layer.msg('验证号码不能为空', {time:1000});
		$('.inp-verification').focus();
		return
	}

	if (mobileReg.test(mobile) && verification !== "") {
		var index = layer.load(2, {
			shade: [0.1,'#fff']
		});
		setTimeout(function() {

			layer.close(index);
			layer.msg('短信发送中，请查收验证码。', {time:1000});
			time();

		},1000);

		
		// $.ajax({
		// 	url: 'http://api.qcplay.com/sms',
		// 	type: 'POST',
		// 	data: 'mobile=' + mobile + '&channel=activation&captcha=' + captcha,
		// 	success: function (json) {
		// 		layer.close(index);
		// 		if (json.result == true) {
		// 			time();
		// 			layer.msg(json.message, {time:1000});
		// 		} else {
		// 			layer.msg(json.message, {time:1000});
		// 		}
		// 	}
		// });
	};



});

// 60s倒计时
var wait=60;
function time() {
	if (wait == 0) {
		$('.obtain-verification a').remove();
		$('.obtain-verification').html("<a>获取手机验证码</a>");
		wait = 60;
	} else {
		$('.obtain-verification a').remove();
		$('.obtain-verification').html("<b>"+wait+"秒</b>");
		wait--;
		setTimeout(function() {time()},1000);
	}
};
